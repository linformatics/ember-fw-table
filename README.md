# ember-fw-table

We have a couple of documentation websites, which may help in using this package:

[Ember FW Table Docs](https://linformatics.bitbucket.io/docs/addons/client/ember-fw-table) give a more robust guide to some of the important features of the package.

[Ember FW Table API Docs](https://linformatics.bitbucket.io/docs/addons/client-api/ember-fw-table) are api documentation for each of our components, which are generated from code comments. 

This README outlines the details of collaborating on this Ember addon.

## Installation

* `git clone <repository-url>` this repository
* `cd ember-fw-table`
* `npm install`

## Running

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).

## Running Tests

* `npm test` (Runs `ember try:each` to test your addon against multiple Ember versions)
* `ember test`
* `ember test --server`

## Building

* `ember build`

For more information on using ember-cli, visit [https://ember-cli.com/](https://ember-cli.com/).
