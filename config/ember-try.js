/* eslint-env node */
module.exports = {
    command: 'ember test --filter="ESLint" -i',
    useYarn: true,
    scenarios: [
        {name: 'ember-lts-2.4'},
        {name: 'ember-lts-2.8'},
        {name: 'ember-release'},
        {name: 'ember-beta'},
        {name: 'ember-canary'},
        {name: 'ember-default'}
    ]
};
