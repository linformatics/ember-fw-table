/* eslint-env node */
'use strict';

module.exports = {
    name: '@bennerinformatics/ember-fw-table',
    included() {
        this._super.included.apply(this, arguments);
    },
    isDeveloppingAddon: () => true
};
