/* Below is the main documentation for the addon. This file is only for documentation, it doesn't actually do anything. */
/**
 * The primary purpose of ember-fw-table is to make a simplified form of ember-light-table,
 *  and which is easier to use than its parent. Within it as well, we have added a few extra
 *  things that make more complicated aspects of tables also easier to incorporate (such as
 *  paginated tables). For more information on ember-light-table,
 *  [see their documentation](https://adopted-ember-addons.github.io/ember-light-table/docs). For a more
 *  comprehensive guide to Ember FW Table, see our [Ember FW Table](https://linformatics.bitbucket.io/docs/addons/client/ember-fw-table).
 *  This is just designed to be API docs to briefly describe various elements and how to use them.
 *
 * @module Introduction
 * @main Introduction
 */
 /**
  * Components defined by Ember FW Table.
  *
  * @module Components
  * @main Components
  */

/**
 * Cell Components in Ember FW Table.
 *
 * For each of these cell components, you call them in the following way:
 * ```js
 * column: [
 *     {
 *         label: 'Actions',
 *         cellComponent: 'fw-cell-action'
 *     }
 * ]
 * ```
 *
 *
 * @module CellComponents
 * @main CellComponents
 */

/**
 * Column Components in Ember FW Table.
 *
 * For each of these column components, call them in the following way:
 *
 * ```js
 * columns: [
 *     {
 *         label: 'Column',
 *         component: 'fw-column-title'
 *     }
 * ]
 *
 * ```
 *
 * @module ColumnComponents
 * @main ColumnComponents
 */

/**
 * Utils defined in Ember FW Table.
 *
 * @module Utils
 * @main Utils
 */
