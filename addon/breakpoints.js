export default {
    mobile:  '(max-width: 650px)',
    tablet:  '(min-width: 651px) and (max-width: 991px)',
    desktop: '(min-width: 992px) and (max-width: 1200px)',
    jumbo:   '(min-width: 1201px)'
};
