import Component from '@ember/component';
import {computed} from '@ember/object';
import {isEmpty} from '@ember/utils';
import {showExpanded} from '../utils/table';
import layout from '../templates/components/fw-table-expanded-rows';
/**
 * This component contains all of the hidden rows of `FwTableExpandedRow`. This is also used completely internally and should not be called directly.
 * @private
 * @class FwTableExpandedRow
 */
export default Component.extend({
    layout,

    columns: computed('table.expandedColumns', 'row', 'row.{canExpand}', function() {
        let row = this.get('row');
        let table = this.get('table');
        return this.get('table.expandedColumns').filter((column) => {
            return showExpanded(table, row, column);
        });
    }),

    hasColumns: computed('columns', function() {
        return !isEmpty(this.get('columns'));
    })
});
