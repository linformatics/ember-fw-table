import Component from '@ember/component';
import {computed} from '@ember/object';
import {empty, filterBy} from '@ember/object/computed';
import {inject} from '@ember/service';
import {isEmpty, isNone} from '@ember/utils';
import {handleAjaxError} from '@bennerinformatics/ember-fw/utils/error';
import exportTable from '@bennerinformatics/ember-fw-table/utils/export';
import Table from 'ember-light-table';
import RSVP from 'rsvp';
import layout from '../templates/components/fw-pagination-wrapper';
/**
 * For an in depth guide on how to implement pagination, including on the server, see our [Paginated Table Concept](https://linformatics.bitbucket.io/docs/addons/client/ember-fw-table/concepts/paginated-table).
 * All of the possible parameters are documented below. At its most basic, however, there are two ways to use `FwPaginationWrapper` in parameter form or in block form. Here is a brief example of each:
 *
 * Parameter form:
 * ```handlebars
 * <FwPaginationWrapper @modelName="myModel" @defaultSortKey="sortKey" @makeQuery={{action "makeQuery"}} @getTitle={{action "getTitle"}} @getTableColumns={{action "getTableColumns"}} @tableActions={{hash ...}} as |actions|>
 *   {{!-- Search panel contents --}}
 *   {{!-- actions.search and actions.export are defined for you to use in this section appropriately--}}
 * </FwPaginationWrapper>
 * ```
 * Block form:
 * ```handlebars
 * <FwPaginationWrapper @modelName="myModel" @defaultSortKey="sortKey" @makeQuery={{action "makeQuery"}} @getTitle={{action "getTitle"}} as |actions table|>
 *   {{#unless table}}
 *      {{!-- Search panel contents --}}
 *      {{!-- actions.search and actions.export are defined for you to use in this section appropriately--}}
 *   {{else}}
 *      {{!-- Table component invocation --}}
 *      {{!-- actions.sort and table.title, table.suffix, table.entries, and table.sortKey are defined for you to use in this section appropriately--}}
 *   {{/unless}}
 * </FwPaginationWrapper>
 * ```
 * @class FwPaginationWrapper
 * @module Components
 */
export default Component.extend({
    layout,
    ajax: inject(),
    config: inject(),
    media: inject(),
    notifications: inject(),
    store: inject(),
    tagName: '',

    didReceiveAttrs() {
        this._super(...arguments);
        if (this.get('searchOnRender')) {
            this.send('search');
        }
    },

    /*
     * Parameters
     */

    /**
     * Name of the model in search
     * @type {String}
     * @property modelName
     */

    /**
      * Whether or not the table loads on page load. If true, it will call the search based on your defaults already set, whenever the table is rendered for the first time.
      * @type {Boolean}
      * @property searchOnRender
      * @default false
      */
    searchOnRender: false,
    /**
     * Default sort order for the table
     * @type {String}
     * @property defaultSortKey
     */
    defaultSortKey: null,

    /**
     * Number of entries per page
     * @type {Number}
     * @property entriesPerPage
     * @default 100
     */
    entriesPerPage: 100,
    /**
     * Show the page numbers above the table. If false, they won't show.
     * @type {Boolean}
     * @property showPagesTop
     * @default true
     */
    showPagesTop: true,

    /**
     * Show the page numbers below the table. If false, they won't show.
     * @type {Boolean}
     * @property showPagesBottom
     * @default true
     */
    showPagesBottom: true,

    /**
     * Gets the title of this table at the given time
     * @method getTitle
     * @type {Action}
     * @return {String} Title of this table
     */
    getTitle() {
        return 'Table';
    },

    /**
     * Gets a list of table columns for exporting
     * @method getTableColumns
     * @type {Action}
     * @return {Array} Array of table columns
     */
    getExportColumns() {},

    /**
     * Action to be called when the table search button is pressed.
     * @type {Function}
     * @method onSearch
     */
    onSearch() {},

    /**
     * Called to delete the full page of entries.
     * Should be passed in with a function. Button will only appear if this is defined
     * @method deletePage
     * @type {Action}
     */
    deletePage: undefined,

    /**
     * Determines permission for deleteTablePermission for `FwTableSortable`. Should probably use a `has-role` helper from Group Control
     * @property deletePagePermission
     * @type {Boolean}
     * @default true
     */
    deletePagePermission: true,
    /**
     * Makes a query object based on the search fields
     * @method makeQuery
     * @type {Action}
     * @param  {Boolean} count  If true, counting
     * @param  {Number}  page   If defined, page number for a page search
     * @param  {Boolean} export If true, exporting
     * @return {Object}         Query object
     */
    makeQuery(/* {count, page, export} */) {
        return {};
    },

    /* Table generation properties */

    /**
     * Gets a list of table columns for the results. If null, yields for table
     * @method getTableColumns
     * @return {Array} Array of table columns
     */
    getTableColumns() {
        return null;
    },

    /**
     * Text to show for empty tables. If null, hides on empty
     * @property emptyText
     * @type {String}
     */
    emptyText: null,

    /**
     * Actions to pass into the table. Should be a hash as usual
     * @property tableActions
     * @type {Object}
     */
    tableActions: null,

    /**
     * Class names for the wrapper around the loading spinner and the table
     * @property tableWrapperClass
     * @type {String}
     */
    tableWrapperClass: '',

    /*
     * Search properties
     */

    /**
     * Table title at this time. Internal property. Set by `getTableTitle`
     * @property currentTitle
     * @type {String}
     * @private
     */
    currentTitle: null,

    /**
     * If true, currently doing the main search. Internal property set by search functionality.
     * @property searchingTable
     * @type {Boolean}
     * @default false
     * @private
     */
    searchingTable: false,

    /**
     * If true, show export page button
     * @property showExport
     * @type {Boolean}
     * @default true
     */
    showExport: true,
    /**
     * Number of pages being searched. Internal property
     * @property pagesSearching
     * @type {Number}
     * @default 0
     * @private
     */
    pagesSearching: 0,

    /**
      * Array of entries, indexes are the pages. Internal Property for more quickly rendering pages that have been searched this time.
      * @property pageEntries
      * @type {Array}
      * @private
      */
    pageEntries: null,

    /**
     * Currently selected page. Internal property to be set by clicking one of the page numbers.
     * @property page
     * @type {Number}
     * @default 1
     * @private
     */
    page: 1,

    /**
     * Total number of entries. Internal property set by the count network request.
     * @property count
     * @type {Number}
     * @private
     */
    count: null,

    /**
     * Query last time createQuery was called
     * @property lastQuery
     */
    lastQuery: null,

    /**
     * Current sort order for the table. Internal property to keep track of current sort key.
     * @type {String}
     * @property currentSortKey
     * @private
     */
    currentSortKey: null,

    /*
     * Computed properties
     */

    /**
     * Array index for the selected page
     * @type {Number - Computed}
     * @property index
     * @internal
     */
    index: computed('page', function() {
        return this.get('page') - 1;
    }),

    /**
     * Array of entries at the current page
     * @type {Array - Computed}
     * @property currentEntries
     * @internal
     */
    currentEntries: computed('pageEntries.[]', 'index', function() {
        let entries = this.get('pageEntries');
        if (isNone(entries)) {
            return [];
        }
        return entries.objectAt(this.get('index'));
    }),

    /**
     * Filtered entries, removing deleted entries
     * @type {Array - Computed}
     * @property filteredEntries
     * @internal
     */
    filteredEntries: filterBy('currentEntries', 'isDeleted', false),

    /**
     * Current table sort key based on given properties
     * @type {String - Computed}
     * @property tableSortKey
     * @internal
     */
    tableSortKey: computed('defaultSortKey', 'currentSortKey', function() {
        let current = this.get('currentSortKey');
        if (isEmpty(current)) {
            return `${this.get('defaultSortKey')}:desc`;
        }
        return current;
    }),

    /**
     * Number of pages available
     * @type {Number - Computed}
     * @property totalPages
     * @internal
     */
    totalPages: computed('count', 'entriesPerPage', function() {
        let count = this.get('count');
        if (isNone(count)) {
            return 0;
        }

        return Math.ceil(count / this.get('entriesPerPage'));
    }),

    /**
     * True if we have multiple pages
     * @type {Boolean - Computed}
     * @property showPages
     * @internal
     */
    showPages: computed('totalPages', function() {
        return this.get('totalPages') > 1;
    }),

    /**
     * Maximum number of pages to show in the pagination component
     * @type {Number - Computed}
     * @property maxPageButtons
     * @internal
     */
    maxPageButtons: computed('media.{isMobile,isTablet}', function() {
        let media = this.get('media');
        if (media.get('isMobile')) {
            return 3;
        }
        if (media.get('isTablet')) {
            return 5;
        }

        return 7;
    }),

    /**
     * Gets the serverside route to use for this model name
     * @type {String - Computed}
     * @property routeName
     * @internal
     */
    routeName: computed('modelName', function() {
        let name = this.get('modelName');
        return this.get('store').adapterFor(name).pathForType(name);
    }),

    /**
     * Final piece of title for table
     * @type {String - Computed}
     * @property tableSuffix
     * @internal
     */
    tableSuffix: computed('count', 'currentEntries', 'index', function() {
        let count = this.get('count');
        let entries = this.get('currentEntries');
        if (isEmpty(entries)) {
            return `${count} entries`;
        }
        return `${entries.get('length')} of ${count} entries`;
    }),

    /**
     * Title for the table
     * @type {String - Computed}
     * @property fullTableTitle
     * @internal
     */
    fullTableTitle: computed('currentTitle', 'tableSuffix', function() {
        return `${this.get('currentTitle')} - ${this.get('tableSuffix')}`;
    }),

    /**
     * If true, hide the table when empty
     * @type {Boolean - Computed}
     * @property hideEmpty
     * @internal
     */
    hideEmpty: empty('emptyText'),

    /* Functions */

    /**
     * Queries the serverside to get the total record count. Internal method.
     * @method queryCount
     * @private
     * @return {Promise} Promise that resolves to a number
     */
    queryCount() {
        // fetch standard query
        let query = this.makeQuery({count: true});
        query.count = true;

        // make request
        let url = this.get('config').formUrl(this.get('routeName'));
        return this.get('ajax').request(url, {data: query}).then((({count}) => count)).catch(handleAjaxError.bind(this));
    },

    /**
     * Query for settign a new sort order. Internal method.
     * @method querySort
     * @private
     * @param  {Number}  page      Page number to start
     * @param  {String}  sortKey   New sort order
     * @param  {Boolean} ascending If true, sorts ascending, false descending
     * @return {Promise}           Promise that resolves to a entry array
     */
    querySort(page, sortKey, ascending) {
        let query = this.get('lastQuery');

        // set sort key stuff if present
        if (!isNone(ascending)) {
            query.ascending = ascending;
        }
        if (!isEmpty(sortKey)) {
            query.sortKey = sortKey;
        }

        // set limits on query
        let entriesPerPage = this.get('entriesPerPage');
        query.limit = entriesPerPage;
        query.offset = (page - 1) * entriesPerPage;

        // make promise
        return RSVP.resolve(this.get('store').query(this.get('modelName'), query)).catch(handleAjaxError.bind(this));
    },

    /**
     * Fetches the entries for the given page number. Internal method
     * @method queryPage
     * @private
     * @param  {Number}  page Page to fetch
     * @return {Promise}      Promise that resolves to an entry array
     */
    queryPage(page) {
        // same as sort, but handles the entries
        return this.querySort(page).then((entries) => {
            this.get('pageEntries')[page - 1] = entries;
            return entries;
        });
    },

    /**
     * Gets all entries for the given query. Internal method
     * @method queryAll
     * @private
     * @return {Promise} Promise that resolves to entries
     */
    queryAll() {
        let query = this.makeQuery({export: true});
        return RSVP.resolve(this.get('store').query(this.get('modelName'), query)).catch(handleAjaxError.bind(this));
    },

    actions: {
        /* Search buttons */

        /**
         * This action is called when the search button is pressed. This is yielded as `actions.search` in the block.
         * @method search
         * @return {Promise} Promise that resolves after searching
         */
        search() {
            // TODO: canSearch?
            // else a title getter

            // start search and clean up old data
            this.setProperties({
                currentTitle: this.getTitle(),
                // search data
                lastQuery: this.makeQuery({page: 1}),
                pageEntries: [],
                page: 1,
                // searching keys
                searchingTable: true,
                pagesSearching: 0,
                tableColumns: this.getTableColumns()
            });

            // search callback
            this.onSearch();

            // make two requests: one for the total count and one for the first 100 entries
            return RSVP.hash({
                count: this.queryCount(),
                entries: this.queryPage(1)
            }).then(({count}) => {
                // entries already set as part of queryPage
                this.setProperties({
                    count,
                    searchingTable: false
                });
            }).catch(() => {
                // request failed, clean up data
                this.setProperties({
                    // search data
                    pageEntries: null,
                    count: 0,
                    // searching keys
                    searchingTable: false
                });
            });
        },

        /**
         * This action is called when the export button is clicked to export all data. This is yielded in the block as `actions.export`.
         * @method export
         * @return {Promise} Promise that resolves after exporting the table
         */
        export() {
            // TODO: canSearch?

            // build table for export
            let table = new Table(this.getExportColumns());
            return this.queryAll().then((entries) => {
                table.setRows(entries.sortBy(this.get('defaultSortKey')).reverse());
                exportTable(table, `${this.getTitle()} - All Entries`);
            }).catch(handleAjaxError.bind(this));
        },

        /* Pagination */

        /**
         * This action is called when a page button is clicked to switch pages. This is an internal action used in the page number button.
         * @method setPage
         * @private
         * @param {Number} page New page number to set
         */
        setPage(page) {
            // clamp page number
            let max = this.get('totalPages');
            if (page < 1) {
                page = 1;
            } else if (page > max) {
                page = max;
            }

            // if we havve entries at the page number, use those
            // if missing, query them
            if (isNone(this.get('pageEntries').objectAt(page - 1))) {
                this.incrementProperty('pagesSearching');
                this.set('page', page);
                this.queryPage(page).then(() => {
                    // entries set in promise logic
                    this.decrementProperty('pagesSearching');
                });
            } else {
                this.set('page', page);
            }
        },

        /* Sorting */

        /**
         * This action resorts the entry by the given column. This is yielded as `actions.sort`, when table is also defined. To be used in the `onSort` of your `FwSortableTable`.
         * @method sortColumn
         * @param  {Column} column  Column to use for sorting
         * @param  {String} sortKey String to use for sorting in the column
         * @return {Promise}        Promise that resolves to entries
         */
        sortColumn(column, sortKey) {
            // if the sort key is unchanged, do nothing
            if (sortKey === this.get('tableSortKey')) {
                return RSVP.resolve();
            }

            // mark that we are sorting, column properties do not add desc
            column.set('sorting', true);

            // search for data
            let page = this.get('page');
            return this.querySort(page, column.searchKey || column.valuePath, column.ascending).then((entries) => {
                // set entries to new list
                let pageEntries = [];
                pageEntries[page - 1] = entries;
                this.setProperties({pageEntries, currentSortKey: sortKey});

                // mark that we are done sorting
                column.set('sorting', false);
                return entries;
            });
        }
    }
});
