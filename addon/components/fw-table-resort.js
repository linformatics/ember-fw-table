import Component from '@ember/component';
import {defineProperty} from '@ember/object';
import {alias, filter, filterBy, sort} from '@ember/object/computed';
import layout from '../templates/components/fw-table-resort';

/**
 * This table contains client-side logic to resort a "table" of models. This resort table will not call the actual serverside "save" function, so you will need to persist the changes to the database yourself,
 * but it will do everything else for you (ie all you should have to do is call save, and the reorder will work). A basic usage example is the following:
 *
 * ```handlebars
 * <FwTableResort @items={{items}} @titleKey='name' @sortKey='sorted' />
 * ```
 *
 * If used in a block format instead, it is possible to fully define the title area, such as to replace it with input fields, which also adds a cheveron to expand if desired
 * ```handlebars
 *  <FwTableResort @items={{items}} @sortKey='sorted' as |item expanded|>
 *      {{input item.name}}
 *  </FwTableResort>
 *  ```
 * Currently the only way to have a display title different from the default sort is to use the block format manually (set the `titleKey` to the desired default sort, and then manually display
 * the other desired property).
 * @class FwTableResort
 * @public
 * @module Components
 */
const TableResort = Component.extend({
    layout,
    tagName: 'section',
    classNames: ['panel-body', 'fw-table-resort'],
    /**
     * Required property. This is what you pass the array of items you wish to sort with the table.
     * @public
     * @property items
     * @type {Boolean}
     */
    /**
    * If true, show an expand button for more fields
    *
    * @public
    * @property allowExpand
    * @type {Boolean}
     */
    allowExpand: false,

    /**
     * Filter called to check if an item is active
     *
     * @public
     * @property activeFilter
     * @type {Function}
     */
    activeFilter: null,

    /**
     * Key used to determine if an item is active
     *
     * @public
     * @property activeKey
     * @type {String}
     */
    activeKey: null,

    /**
     * If defined with an action, adds a delete button to delete items
     *
     * @public
     * @default true
     * @property delete
     * @type {Action}
     */
    delete: () => {},

    /**
     * The icon that will be used for the delete button. Useful if you want the button to do something other than delete.
     *
     * @public
     * @default fa-regular fa-trash-can
     * @property deleteIcon
     * @type {String}
     */
    deleteIcon: 'fa-regular fa-trash-can',

    /**
     * String pointer to the model's sort key, will be used to sort the items and modify the key later
     *
     * @public
     * @property sortKey
     * @type {String}
     */
    sortKey: '',

    /**
     * Default sort for if the sort key is undefined and title to display if no block is used
     * @public
     * @property title
     * @type {String}
     */
    titleKey: null,

    /**
     * Spacing between sortable items
     *
     * @public
     * @property spacing
     * @type {Number}
     */
    spacing: 15,

    /**
     * Tolerance before dragging starts
     *
     * @public
     * @property distance
     * @type {Number}
     */
    distance: 0,

    /**
     * An internal list of all items which will be shown in the table (takes into account activeKey and activeFilter)
     *
     * @private
     * @property viewableItems
     * @type {Array}
     */

    /**
     * Sort order as defined by passed properties
     * @private
     * @property _sortOrder
     * @type {Array}
     */
    _sortOrder: null,

    /**
     * Sorted list of items in the table
     *
     * @private
     * @property sortedItems
     * @type {Array}
     */
    sortedItems: sort('viewableItems', '_sortOrder'),

    didReceiveAttrs() {
        this._super(...arguments);

        // determine the default sort order from the sortkey
        this.set('_sortOrder', [this.get('sortKey')]);
        // if we have a fallback for default is null, add that as well
        if (this.get('titleKey')) {
            this.get('_sortOrder').pushObject(this.get('titleKey'));
        }

        // determine how we will filter the viewable items based on what the user gave
        // if its a key, either filter type
        let activeKey = this.get('activeKey');
        if (activeKey) {
            // have a function? use that
            let activeFilter = this.get('activeFilter');
            if (activeFilter) {
                defineProperty(this, 'viewableItems', filter(`items.@each.${activeKey}`, activeFilter));
            } else {
                // otherwise do a flat filterBy
                defineProperty(this, 'viewableItems', filterBy('items', activeKey));
            }
        } else {
            // no key? just display all
            defineProperty(this, 'viewableItems', alias('items'));
        }
    },

    /**
     * Helper function to sort a list of passed items
     * @method reorderItems
     * @param  {DS.Model[]} items  Model array
     * @private
     */
    reorderItems(items) {
        let activeFilter = this.get('activeFilter');
        if (activeFilter) {
            items = items.filter(activeFilter);
        }
        items.forEach((item, index) => {
            item.set(this.get('sortKey'), index + 1);
        });
    },

    actions: {
        /**
         * Called when the expand button is clicked on any row
         * @param  {DS.Model} item  Model clicked
         */
        toggleExpanded(item) {
            item.toggleProperty('expanded');
        },

        /**
         * Called whenever an item is moved to reorders items based on their index in the array
         * @param {DS.Model[]} items The items to reorder
         */
        reorderItems(items) {
            this.reorderItems(items);
        },

        /**
         * Called when the delete button is pressed to redirect that call to the passed in action
         * @param  {Model} item Model to delete
         */
        deleteItem(item) {
            this.delete(item);
            this.reorderItems(this.get('sortedItems'));
        }
    }
});

TableResort.reopenClass({
    positionalParams: ['items']
});

export default TableResort;
