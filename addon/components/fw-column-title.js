import Sortable from './fw-column-sortable';
import layout from '../templates/components/fw-column-title';

/**
 * This column component extends [`fw-column-sortable` component](../classes/FwColumnSortable.html), so it will display a sort-icon, but it also display a column title
 * which will truncate with an ellipsis if too long. This can be used for your columns when using the [FwPaginationWrapper](../classes/FwPaginationWrapper.html),
 * so that sorting works properly. It should be set as the columns component (not the cellComponent), like so:
 * ```js
 *  columns: [{
 *   label: 'Name',
 *   valuePath: 'name',
 *   component: 'fw-column-title'
 *  }...]
 * ```
 * @public
 * @class FwColumnTitle
 * @module ColumnComponents
 */
export default Sortable.extend({
    layout
});
