import Component from '@ember/component';
import {computed} from '@ember/object';
import layout from '../templates/components/fw-column-sortable';

/**
 * Displays a column title which can show a sorting icon if a network request is still active. This can be used for your columns when using the [FwPaginationWrapper](../classes/FwPaginationWrapper.html),
 * so that sorting works properly. It should be set as the columns component (not the cellComponent), like so:
 * ```js
 *  columns: [{
 *   label: 'Name',
 *   valuePath: 'name',
 *   component: 'fw-column-sortable'
 *  }...]
 * ```
 * If you do this, when you are sorting the table within the paginated wrapper, it will display a spinning icon letting the user know that the sort is loading.
 * @public
 * @class FwColumnSortable
 * @module ColumnComponents
 */
export default Component.extend({
    layout,

    /**
     * Computed property that determines the icon of the sort whether it is currently sorting, sortable, ascending, or sorted.
     * @property sortIcon
     * @type {String|null}
     * @private
     */
    sortIcon: computed('column.{sorting,sortable,sorted,ascending}', function() {
        if (this.get('column.sorting')) {
            return 'fa-solid fa-spin fa-rotate';
        }

        let name;
        if (this.get('column.sorted')) {
            name = this.get('column.ascending') ? 'iconAscending' : 'iconDescending';
        } else {
            name = this.get('column.sortable') ? 'iconSortable' : null;
        }

        return name && this.get(`sortIcons.${name}`);
    })
});
