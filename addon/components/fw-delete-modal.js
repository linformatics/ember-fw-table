import Component from '@ember/component';
import {computed} from '@ember/object';
import layout from '../templates/components/fw-delete-modal';
import RSVP from 'rsvp';
/**
 * The FwDeleteModal component is a pseudo component/modal hybrid because it opens a modal and internally uses the `FwFullscreenModal` component, specifically
 * the [`confirm-choice` modal](https://linformatics.bitbucket.io/docs/addons/client-api/ember-fw/classes/Confirm-ChoiceModal.html) in the "sm" size. However, `FwDeleteModal` requires even less
 * set up than that, thus it has the extremely niche case of opening a delete modal, and cannot really be expanded even to an "are you sure you want to archive?" modal. However, it
 * is very useful for what it is intended. How you use it is you follow the instructions for actions in [Calling the Modal](https://linformatics.bitbucket.io/docs/addons/client/ember-fw/concepts/modals#calling-the-modal), but using
 * `FwDeleteModal` instead of `FwFullscreenModal`. The only properties you can pass in to either `confirm-choice` or `FwFullscreenModal` itself are those listed below. An example of how to use it would be:
 * ```handlebars
 * {{#if shouldRender}}
 *    <FwDeleteModal @model={{modelToDelete}} @type="modelName" @close={{action "toggleShouldRender"}} />
 * {{/if}}
 * ```
 * When the user confirms the choice to delete, this component will automatically call `destroyRecord` on that model, deleting it for you.
 * @public
 * @class FwDeleteModal
 * @module Components
 */
export default Component.extend({
    layout,
    /**
     * Action called to close the modal.
     * @property close
     * @type {Action}
     */
    close: null,

    /**
     * Model which will be deleted.
     * @property model
     * @type {DS.Model}
     */
    model: null,

    /**
     * Message displayed in the confirmation. If unset use the default (which looks at type property).
     * @property message
     * @default "Are you sure you want to delete this {type}? It cannot be undone."
     * @type {String}
     */
    message: null,

    /**
     * Model type being deleted, used to create the message.
     * @property type
     * @type {String}
     */
    type: null,

    /**
     * Determines whether opening this modal caused the web browser to scroll to the top.
     * @property scrollTop
     * @default false
     * @type {Boolean}
     */
    scrollTop: false,

    _message: computed('message', 'type', function() {
        // if we have a message, use that
        let message = this.get('message');
        if (message) {
            return message;
        }

        // if no message, determine the type and use the default
        let type = this.get('type') || 'model';
        return `Are you sure you want to delete this ${type}? It cannot be undone.`;
    }),

    actions: {
        // deletes the contained model
        deleteModel() {
            if (this.get('model')) {
                return RSVP.resolve(this.get('model').destroyRecord());
            }
        }
    }
});
