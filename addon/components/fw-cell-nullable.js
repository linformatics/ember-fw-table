import Component from '@ember/component';
import layout from '../templates/components/fw-cell-nullable';

/**
 * Creates a table cell that displays a default value if null.
 * That value defaults to "None", but can be changed by setting the `nullText` property in a column.
 * @deprecated You should use the [`formatNullable` in the Format Util](../classes/BaseCells.html) instead.
 * @public
 * @class FwCellNullable
 * @module CellComponents
 *
 */
export default Component.extend({
    layout
});
