import Component from '@ember/component';
import layout from '../templates/components/fw-cell-action';

/**
 * This cell contains buttons to edit and delete a model, which can have actions passed in.
 * The edit action is expected to be passed in as `tableActions.edit`, while delete as `tableActions.delete`.
 * This component will only give you two actions, so if you need a more complicated component than edit and delete,
 * you will need to create your own cell component.
 * Setting the variable `small` in the column will cause the buttons to display smaller.
 *
 * The easiest way to add this cell is through the base cell in utils, for more information, [click here](../classes/BaseCells.html). In most cases, it is best
 * to use the util rather than this cell component directly.
 * @public
 * @class FwCellAction
 * @module CellComponents
 */
export default Component.extend({
    layout
});
