import Component from '@ember/component';
import {computed} from '@ember/object';
import layout from '../templates/components/fw-row-toggle';

/**
 * Creates a table cell that shows if the row is expanded or hidden. See also [`FwRowToggleIndex`](FwRowToggleIndex.html).
 *
 * @deprecated While this can be called directly using `cellComponent`, you should use the [`rowToggle` util](../classes/BaseCells.html) import, which will display this component if there is no `valuePath` defined.
 * @class FwRowToggle
 * @module CellComponents
 */
export default Component.extend({
    layout,
    /**
     * Computed property used internally to determine whether the "open" or "closed" toggle will be shown based on whether the row is expanded or not
     * @private
     * @property toggleClass
     * @type {Computed}
     * @return {String} This returns the appropriate Fontawesome class.
     */
    toggleClass: computed('row.{canExpand,expanded}', function () {
        let classes;
        if (!this.get('row.canExpand')) {
            classes = 'fa-chevron-right faded';
        } else {
            let chevron = this.get('row.expanded') ? 'fa-chevron-down' : 'fa-chevron-right';
            classes = `${chevron} color-text`;
        }

        return `fa-solid ${classes}`;
    })
});
