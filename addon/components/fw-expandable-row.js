import LightRow from 'ember-light-table/components/lt-row';

/**
 * This is a component internal to ember-fw-table, and should not be called outside. It extends the `lt-row` component from Ember Light Table, and simply allows it to be expandable.
 * @private
 * @class FwExpandableRow
 */
export default LightRow.extend({
    classNameBindings: ['row.canExpand:is-expandable']
});
