import Component from '@ember/component';
import {computed} from '@ember/object';
import layout from '../templates/components/fw-table-expanded-row';

/**
 * This component is used internally as the logic behind the rows displayed inside rows.
 * It is not included in the exports as it is not intended to be called directly.
 * @private
 * @class FwTableExpandedRow
 */
export default Component.extend({
    layout,

    value: computed('rawValue', function() {
        let rawValue = this.get('rawValue');
        let format = this.get('column.format');

        if (format && typeof format === 'function') {
            return format.call(this, rawValue);
        }
        return rawValue;
    })
});
