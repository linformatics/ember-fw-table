import {isArray} from '@ember/array';
import Component from '@ember/component';
import {computed, observer} from '@ember/object';
import {sort} from '@ember/object/computed';
import {isEmpty, isNone} from '@ember/utils';
import newTable from '../classes/Table';
import exportTable from '../utils/export';
import layout from '../templates/components/fw-table-sortable';
import RSVP from 'rsvp';

/**
 * This table contains logic to show a table as a panel in an app. This table features sorting rows can be expanded
 * to show hidden data
 *  ```handlebars
 * <FwTableSortable @data={{model}} @columns={{columns}} @title='Table' />
 * ```
 *
 * If used in a block format instead, it is possible to fully define the title area, replacing the title parameter. The export action is passed as a yield parameter to redefine the export button
 *  ```handlebars
 *  <FwTableSortable @data={{model}} @columns={{columns}} as |export|>
 *      <strong class='panel-title'>Table</strong>
 *  </FwTableSortable>
 *  ```
 *
 * In addition to the usual properties in columns, fw-table-sortable supports adding an additional `sortKey` property
 * If defined, clicking that column will sort by that property instead of valuePath
 *
 * It also supports a showExpanded property, which is set to a value which will cause this row to show in the expanded view if hidden.
 * Alternatively, setting this to `true` or `false` will force the state regardless of a property.
 * By default will show if the property from `valuePath` is not null
 *
 * @class FwTableSortable
 * @module Components
 * @extends EmberLightTable
 * @public
 */
const TableSortable = Component.extend({
    layout,
    classNames: ['panel', 'panel-default'],

    /**
     * Table rows
     *
     * @public
     * @property data
     * @type {Array}
     */

    /**
     * Table columns. See ember-light-table docs for more information
     *
     * @public
     * @property columns
     * @type {Array}
     */
    columns: null,

    /**
     * Additional class names for the table
     *
     * @public
     * @property tableClassNames
     * @type {Hash}
     */
    tableClassNames: '',

    /**
     * Hash of maximum columns based on responsive size
     *
     * @public
     * @property breakpoints
     * @type {Hash}
     */

    /**
     * Hash of actions to pass into the table for use in components
     *
     * @public
     * @property tableActions
     * @type {Hash}
     */

    /**
     * Title to display in the panel above the table
     *
     * @public
     * @property title
     * @type {String}
     */

    /**
     * Text to display when the table is empty
     *
     * @public
     * @property empty
     * @type {String}
     */

    /**
     * Action called when sorting the table
     *
     * @public
     * @property onSort
     * @type {Action}
     */
    onSort: () => RSVP.resolve(),

    /**
     * Set to false to make the table not show the header if empty
     *
     * @public
     * @property headerEmpty
     * @type {Boolean}
     * @default true
     */
    headerEmpty: true,
    /**
     * Sets the table to a fixed height, and adds a scroll bar to see the hidden rows
     *
     * @public
     * @property fixedHeight
     * @type {String}
     */
    fixedHeight: null,
    /**
     * Makes the table responsive to the screen width. The table is slightly more efficient if false when responsive is not used
     *
     * @public
     * @property responsive
     * @type {Boolean}
     * @default false
     */
    responsive: false,

    /**
     * Makes the headers able to be dragged to resize. You will still need to put `resizable: true` in each column you want to be able to resize.
     * @public
     * @property resizeOnDrag
     * @type {Boolean}
     * @default false
     */

    resizeOnDrag: false,
    /**
     * Allows exporting the current contents of the table using an export button. Note that this sets a default title of "Table", so setting a separate one is recommended
     *
     * When enabled, columns have some additional values defined:
     * * `canExport`: if true (default), the column will be included in the export. Set to false to skip exporting the column.
     * * `exportOnly`: if true, the column will not show in the table, but will still be exported.
     *
     * Additionally, the value `export` is set to true in `format`'s context.
     *
     * If this is set to a string, the string is used as the name of the export button.
     * @public
     * @property canExport
     * @type {Boolean | String}
     * @default false
     */
    canExport: false,

    /**
     * If true, uses the table block data for the header
     *
     * @public
     * @property showHeader
     * @type {Boolean}
     * @default true
     */
    showHeader: true,

    /**
     * If true, uses the table block data for the footer
     *
     * @public
     * @property showFooter
     * @type {Boolean}
     * @default true
     */
    showFooter: false,

    /**
     * Default sort order for the table. Can be a string for a single property, or an array to fallback
     *
     * @public
     * @property defaultSort
     * @type {String|Array}
     */
    defaultSort: null,

    /**
     * Private copy of defaultSort, made an array if its not
     *
     * @private
     * @property _defaultSort
     * @type {Array}
     */
    _defaultSort: computed('defaultSort', function() {
        let defaultSort = this.get('defaultSort');
        if (isNone(defaultSort)) {
            return [];
        }
        if (!isArray(defaultSort)) {
            return [defaultSort];
        }
        return defaultSort;
    }),

    /**
     * Sort column set by clicking a column to sort
     *
     * @private
     * @property sortColumn
     * @type {String}
     */
    sortColumn: null,

    /**
     * Internal sort order. Calculated from sortColumn and _defaultSort
     *
     * @private
     * @property sortOrder
     * @type {Array}
     */
    sortOrder: computed('_defaultSort.[]', 'sortColumn', function() {
        let sortColumn = this.get('sortColumn');
        if (sortColumn) {
            return [sortColumn].pushObjects(this.get('_defaultSort'));
        }
        return this.get('_defaultSort');
    }),

    /**
     * Passed in table data after sorting.
     *
     * @private
     * @property sortedData
     * @type {Array}
     */
    sortedData: sort('data', 'sortOrder'),

    /**
     * Internal table instance, in general changes to the table should be done to columns or data so they update propery
     *
     * @protected
     * @property table
     * @type {Table}
     */
    table: null,

    /**
     * Computed table title based on whether a boolean true is passed or a title
     *
     * @private
     * @property exportTitle
     * @type {String}
     */
    exportTitle: computed('canExport', function() {
        let canExport = this.get('canExport');
        if (canExport === true) {
            return 'Export';
        }
        return canExport || '';
    }),

    /**
     * Called to delete the full page of entries. Pass in the action to be called by the button.
     *
     * @property deleteTable
     * @type {Action}
     * @default undefined
     */
    deleteTable: undefined,

    /**
     * Determines title for delete function. This will only be used if deleteTable action is passed in.
     *
     * @property deleteOverrideTitle
     * @type {String}
     */
    deleteOverrideTitle: null,

    deleteTitle: computed('deleteTable', 'deleteOverrideTitle', function() {
        if (!this.get('deleteTable')) {
            return null;
        } else {
            return this.get('deleteOverrideTitle') ? this.get('deleteOverrideTitle') : 'Delete All';
        }
    }),

    /**
     * Determines if the current user has permission to delete the table (usually has-role helper will be used to determine this)
     * example: `@deleteTablePermission={{has-role 'admin'}}`. This will only be used if `deleteTable` action is passed in.
     * @property deleteTablePermissions
     * @type {Boolean}
     * @default true
     */
    deleteTablePermission: true,

    /**
     * Callback to update the table rows whenever the data is changed, generally from sorting
     *
     * @private
     * @method updateTableRows
     */
    // eslint-disable-next-line ember/no-observers
    updateTableRows: observer('sortedData', function() {
        if (this.get('table')) {
            this.get('table').setRows(this.get('sortedData'));
        }
    }),

    /**
     * Callback to update the table columns whenever they change. Unlikely, but still a case worth covering
     *
     * @private
     * @method updateTableColumns
     */
    // eslint-disable-next-line ember/no-observers
    updateTableColumns: observer('columns', function() {
        if (this.get('table')) {
            this.get('table').setColumns(this.get('columns'));
        }
    }),

    /**
     * Initializes the component and loads the modal path.
     *
     * @private
     * @method didReceiveAttrs
     */
    didReceiveAttrs() {
        this._super(...arguments);
        // first, determine our sorting
        let defaultSort = this.get('_defaultSort');
        let columns = this.get('columns') || [];
        if (!isEmpty(defaultSort)) {
            // make a map of key to ascending
            let [mainKey] = defaultSort;
            let ascending = true;
            if (mainKey.endsWith(':desc')) {
                ascending = false;
                mainKey = mainKey.substring(0, mainKey.length - 5);
            }
            columns = columns.map((column) => {
                let key = column.sortKey || column.valuePath;
                return key === mainKey ? Object.assign({}, column, {sorted: true, ascending}) : column;
            });
        }
        this.set('table', newTable(columns, this.get('sortedData')));

        if (this.get('noPanel')) {
            this.set('classNames', null);
        }
    },

    actions: {
        /**
         * Called when clicking aa column header to sort the current row
         * @param  {Column} column Column clicked
         */
        sort(column) {
            if (!column.sortable) {
                return;
            }

            // modify the sort state, this is ported from the default code to properly set ascending to true on sort
            if (column.sorted) {
                column.toggleProperty('ascending');
            } else {
                this.get('table.sortedColumns').setEach('sorted', false);
                column.set('sorted', true);
                column.set('ascending', true);
            }

            // determine how we are sorting
            let sortKey = column.sortKey || column.valuePath;

            // if the column is now decending, sort decending
            if (!column.ascending) {
                sortKey = `${sortKey}:desc`;
            }

            // call the sort logic
            this.onSort(column, sortKey).then(() => {
                this.set('sortColumn', sortKey);
            });
        },

        /**
         * Called when clicking a row to expand it and show more information
         * @param  {Row} row Row clicked
         */
        expand(row) {
            let expand = row.get('canExpand') ? !row.get('expanded') : false;
            if (expand) {
                this.get('table.expandedRows').setEach('expanded', false);
            }
            row.set('expanded', expand);
        },

        /**
         * Called by the export button to export the current table data to CSV
         */
        export() {
            exportTable(this.get('table'), this.get('title'));
        }
    }
});

TableSortable.reopenClass({
    positionalParams: ['data', 'columns']
});

export default TableSortable;
