import Component from '@ember/component';
import layout from '../templates/components/fw-cell-permission-icon';

/**
 * This component displays the permission icon for the table on the Permissions page. It should be used in the following way:
 * ```js
 * columns: [{
 *   label: 'Admin',
 *   valuePath: 'admin',
 *   cellComponent: 'fw-cell-permission-icon',
 *   align: 'center',
 *   width: '80px'
 * }]
 * ```
 * This component will take the value, and display the proper Fontawesome icon based on its value. The value should be one of the following three numbers:
 *
 * - 0 means that that role does not have access to that feature, and it will display a red X.
 * - 1 means that that feature is controlled by access control, not by Group Control, and it will display a lock icon in the primary color of the app
 * - 2 means that that role has access to that feature, and will display a green check mark.
 * - If the value is anything else, the cell will just be empty
 *
 * @public
 * @class FwCellPermissionIcon
 * @module CellComponents
 */
export default Component.extend({
    layout
});
