import Component from '@ember/component';
import layout from '../templates/components/fw-cell-boolean';

/**
 * Displays a cell containing a boolean as either "Yes" or "No".
 * @public
 * @deprecated You should use the [`formatBoolean` in the Format Util](../classes/BaseCells.html) instead.
 * @class FwCellBoolean
 * @module CellComponents
 */
export default Component.extend({
    layout
});
