import Toggle from './fw-row-toggle';
import layout from '../templates/components/fw-row-toggle-index';

/**
 * Creates a table cell that shows if the row is expanded or hidden on hover, but
 * displays the value otherwise. See also [`FwRowToggle`](FwRowToggle.html).
 * @public
 * @deprecated While this can be called directly using `cellComponent`, you should use the [`rowToggle` util](../classes/BaseCells.html) import, which will display this component if there is a `valuePath` defined.
 * @class FwRowToggleIndex
 * @module CellComponents
 */
export default Toggle.extend({
    layout
});
