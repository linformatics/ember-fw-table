import {isEmpty} from '@ember/utils';
/**
 * This is an internal util that is used to help with the logic for expanded rows.
 * @class TableUtil
 * @module Utils
 */
/**
 * Checks if a row should be show in expanded view given the context
 * @method showExpanded
 * @param  {Table}  table  Table class instance
 * @param  {Row}    row    Row class instance
 * @param  {Column} column Column class instance
 * @return {boolean}       True if it is shown in this context, false otherwise
 */
export function showExpanded(table, row, column) {
    // if show expanded is defined, use that
    let showExpanded = column.get('showExpanded');
    let showExpandedType = typeof showExpanded;
    // flat boolean? just return
    if (showExpandedType === 'boolean') {
        return showExpanded;
    }
    // key? fetch from the row
    if (showExpandedType === 'string') {
        return row.get(showExpanded);
    }

    // no value path? show it
    let valuePath = column.get('valuePath');
    if (isEmpty(valuePath)) {
        return true;
    }

    // hides undefined, null, and empty arrays, but not false and 0
    return !isEmpty(row.get(valuePath));
}
