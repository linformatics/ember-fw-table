import EmberObject from '@ember/object';
import {isEmpty, isNone} from '@ember/utils';
import Papa from 'papaparse';
/**
 * An internal util that is used in ember-fw-table for the export functionality, both in the `FwTableSortable` and `FwPaginationWrapper`.
 * @class Export
 * @module Utils
 */
/**
 * Creates a CSV file from a data array and a title
 * @method createCSV
 * @param  {Array} data        Array of table cells
 * @param  {Title} [name=null] Name to export
 */
export function createCSV(data, name = null) {
    // first step, we need to extra the cells into "Cell","Cell 2" format, use JSON.stringify as it makes most escapes
    // regex removes the brackets, as its an array we are calling stringify on
    let rows = Papa.unparse(data);

    // next, join that using newlines and export as data CSV
    let encodedUri = `data:text/csv;charset=utf-8,${encodeURIComponent(rows)}`;

    // downloading is a bit weird. We create a link element with the name and URL then click the element
    let link = document.createElement('a');
    link.setAttribute('href', encodedUri);
    link.setAttribute('download', `${name || 'Export'}.csv`);
    document.body.appendChild(link); // Required for FF
    link.click(); // This will download the data file
}

/**
 * Exports an Ember Light Table to CSV
 * @method exportTable
 * @param  {Table} table             Table class, just needs to include rows and columns
 * @param  {String} [title='Export'] Title to use for exporting
 */
export default function exportTable(table, title = 'Export') {
    // start by filtering out columns which opted out of exporting. Examples include action buttons and the row toggle
    let columns = table.get('columns').filter(({canExport}) => {
        // if canExport is defined, require it to be true
        return isNone(canExport) || canExport;
    });

    // ensure we have columns and data
    if (isEmpty(columns)) {
        return;
    }

    // start the data with just the labels from the columns
    let data = [columns.map(({exportLabel, label}) => exportLabel || label)];

    // next, populate data by iterating through the current rows
    table.get('rows').forEach((row) => {
        // in each row, iterate through the columns
        let rowData = columns.map((column) => {
            let {valuePath, format} = column;
            // flat value pulled from the model
            let rawValue = valuePath ? row.get(valuePath) : '';
            let value = rawValue;
            // if we have a format function and a value, run the function
            if (format && typeof format === 'function') {
                // so, typically there is a cell object which contains data as the functions "this"
                // but we are in a action so simulate it by passing in the data we expect the function to have
                // while we are here, might as well pass in an export parameter so they can modify behavior for export if desired
                value = format.call(EmberObject.create({column, row, table, rawValue, export: true}), rawValue);
            }

            // return the value, though convert null/undefined into a string
            return isNone(value) ? '' : value;
        });

        // add that data into the main array
        data.pushObject(rowData);
    });

    // finally, pass our 2 dimensional array to the function to convert to a file download
    createCSV(data, title);
}
