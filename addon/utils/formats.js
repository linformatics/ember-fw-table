/* global moment */
import {htmlSafe} from '@ember/string';
import {isNone} from '@ember/utils';
/**
 * These utils are designed for helping you be able to easily format a column by passing this function into the `format` attribute of the column. To use these functions, all you need to do
 * is import them at the top of the Javascript file, with the following import:
 * ```js
 * import {functionName} from '@bennerinformatics/ember-fw-table/utils/formats';
 * ```
 * Then you use it in the column array to format it appropriately. Each function gives you an example of how to use it.
 * @class Format
 * @module Utils
 */
/**
 * Format function which replaces the value with a basic Yes/No format for if its true or false. Designed to be used in the following way with a boolean column (after importing `formatBoolean` in the way described above):
 * ```js
 * column: [{
 *   label: 'My Boolean',
 *   valuePath: 'myBoolean',
 *   format: formatBoolean
 *   ...
 * }...],
 * @method formatBoolean
 * @param  {Any} value Value to test
 * @return {String}    "Yes" if the value is truthy, "No" otherwise
 */
export function formatBoolean(value) {
    return value ? 'Yes' : 'No';
}

/**
 * Generates a format function which returns the value or an italic default text if the value is null. Designed to be used in the following way with a column that may have a null value (after importing `formatNullable` in the way described above):
 * ```js
 * column: [
 *  //if null, will display the default "None" in italics
 *   {
 *      label: 'Some Column',
 *      valuePath: 'someColumn',
 *      format: formatNullable
 *      ...
 *   },
 *   //if null, will display "No Value" instead of "None" in italics
 *   {
 *      label: 'Some Column',
 *      valueText: 'someColumn',
 *      format: formatNullable('No Value')
 *  }
 * ...],
 * @method formatNullable
 * @param  {String} [nullText='None'] Text if the value is null
 * @return {Function}                 Function to pass into the column format
 */
export function formatNullable(nullText = 'None') {
    return function(value) {
        if (isNone(value)) {
            return this.get('export') ? '' : htmlSafe(`<i>${nullText}</i>`);
        }
        return value;
    };
}

/**
 * Generates a function which formats a moment based on the specified format. Designed to be used in the following way with a column displaying a date (after importing `formatMoment` in the way described above):
 * ```js
 * column: [
 *   //will display date as 1970-01-01
 *   {
 *      label: 'Some Column',
 *      valuePath: 'someColumn',
 *      format: formatMoment('YYYY-MM-DD')
 *      ...
 *   },
 *   //will display date in the table as 1970-01-01, but when exporting display 01-01-1970
 *   {
 *      label: 'Some Column',
 *      valueText: 'someColumn',
 *      format: formatMoment('YYYY-MM-DD', 'MM-DD-YYYY')
 *  }
 * ...],
 * @method formatMoment
 * @param  {String} format       moment format. For a list of valid strings, see [Moment docs](https://momentjs.com/docs/#/displaying/format/)
 * @param  {String} exportFormat moment format used on exporting
 * @return {Function}      Function to pass into a column which formats by a moment
 */
export function formatMoment(format, exportFormat = null) {
    return function(date) {
        if (moment.isMoment(date)) {
            // if we are exporting and have an export format, use that
            let useFormat = exportFormat && this.get('export') ? exportFormat : format;
            return date.format(useFormat);
        }
    };
}
