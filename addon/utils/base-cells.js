/**
 * This util is designed to create the most basic defined cells for you. While it does not cover all the cellComponents, the most commonly used (`FwRowToggle`, `FwRowToggleIndex`, and `FwCellAction`) can be imported
 * rather than creating the cell object yourself. You would import them in the following way:
 * ```js
 *  import {functionName} from '@bennerinformatics/ember-fw-table/base-cells';
 * ```
 * After it is imported, the functions below tell you how to implement each one.
 * @class BaseCells
 * @module Utils
 */
/**
 * Creates a generic row toggle object. If you pass in a `valuePath` to extra, it will format using the `FwRowToggleIndex` component (which displays a value on hover) rather than just the regular
 * `FwRowToggle`. This should be the first cell defined in the columns arraw. An example use case would be the following (after importing `rowToggle` as defined above):
 * ```js
 * //generic rowToggle, no differences
 * columns: [
 *   rowToggle(),
 *   ...
 * ],
 * //rowToggle when you need to pass in something to extra, such as valuePath or breakpoints
 * columns: [
 *   rowToggle({breakpoints: ['mobile']}),
 *   ...
 * ]
 * ```
 * @method rowToggle
 * @param  {Object} extra Additional values to add to the row toggle. Breakpoints is the most common one, but will accept any normal column inputs
 * @return {Object}       Object to use as a row toggle column
 */
export function rowToggle(extra = {}) {
    let {valuePath} = extra;
    return Object.assign({
        label: valuePath ? '#' : '',
        width: valuePath ? '40px' : '35px',
        align: valuePath ? 'toggle' : 'center',
        sortable: !!valuePath,
        canExport: false,
        showExpanded: false,
        cellComponent: valuePath ? 'fw-row-toggle-index' : 'fw-row-toggle'
    }, extra);
}

/**
 * Creates a generic action cell, with a delete and edit button. Passing `small` into extra, will display as only icon buttons, whereas the full text will be displayed if it is not set.
 * This should always be the last cell in your columns array. An example use case would be the following (after importing as above):
 * ```js
 * //will render action cell with icon buttons only
 * columns: [
 *   ...
 *   actions({small: true})
 * ],
 * //will render action cell with full text of "Edit" and "Delete"
 * columns: [
 *   ...
 *   actions()
 * ]
 * ```
 *
 * **Note**: You will still need to pass in your edit and delete actions to the `tableActions` in the `FwTableSortable` component in order for this to work properly. This only renders the cell.
 * @method actions
 * @param  {Object} extra Additional values to add to the action buttons. Small is useful to shrink it with breakpoints.
 * @return {Object}       Object to use as an actions button column
 */
export function actions(extra = {}) {
    let {small} = extra;
    return Object.assign({
        label: 'Actions',
        // if small is set, shrink the width
        width: small ? '95px' : '170px',
        sortable: false,
        canExport: false,
        showExpanded: false,
        cellComponent: 'fw-cell-action'
    }, extra);
}
