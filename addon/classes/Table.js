import LightTable from 'ember-light-table/classes/Table';
import Row from './Row';
import {A as emberArray} from '@ember/array';
import {computed} from '@ember/object';
import {filter} from '@ember/object/computed';
import {isEmpty, isNone} from '@ember/utils';

/**
 * Create a collection of Row objects with the given collection
 * @method createRows
 * @static
 * @param  {Array}  rows     Passed in rows
 * @param  {Table}  table    Table instance
 * @return {Array}           Array of table rows
 */
function createRows(rows = [], table) {
    return rows.map((r) => new Row({content: r, table}));
}

/**
 * Personal copy of the table class with some additional computed properties
 *
 * @class Table
 * @extends EmberLightTable/Table
 */
/*
 * TODO: needs to implement some additional row methods as right now only setRows is wrapped
 */
export class Table extends LightTable.extend({
    /**
     * Table columns without export only columns
      * @property tableColumns
      * @type {Ember.Array}
     */
    tableColumns: filter('columns.@each.exportOnly', function(column) {
        return !column.get('exportOnly');
    }),

    /**
     * @property visibleColumnGroups
     * @type {Ember.Array}
     */
    visibleColumnGroups: computed('tableColumns.[]', 'tableColumns.@each.{isHidden,isVisibleGroupColumn}', function() {
        return this.get('tableColumns').filter((c) => {
            return c.get('isVisibleGroupColumn') || (!c.get('isGroupColumn') && !c.get('isHidden'));
        });
    }).readOnly(),

    /**
     * @property visibleSubColumns
     * @type {Ember.Array}
     */
    visibleSubColumns: computed('tableColumns.@each.visibleSubColumns', function() {
        return emberArray([].concat(...this.get('tableColumns').getEach('visibleSubColumns')));
    }).readOnly(),

    /**
     * @property allColumns
     * @type {Ember.Array}
     */
    allColumns: computed('tableColumns.@each.subColumns', function() {
        return this.get('tableColumns').reduce((arr, c) => {
            arr.pushObjects(c.get('isGroupColumn') ? c.get('subColumns') : [c]);
            return arr;
        }, emberArray([]));
    }).readOnly(),

    /**
     * List of all columns that can show in expanded view, it will be filtered again later
     *
     * @private
     * @property expandedColumns
     * @type {Array}
     */
    expandedColumns: filter('allColumns.@each.{isHidden,showExpanded,cellComponent}', function(column) {
        // if the column is not hidden, skip it
        if (!column.get('isHidden')) {
            return false;
        }
        // if hidden, ask the column if we should show it
        let showExpanded = column.get('showExpanded');
        if (!isNone(showExpanded)) {
            return showExpanded;
        }

        // by default, show if the value path is empty and its not a row toggle
        return column.get('cellComponent') !== 'fw-row-toggle';
    }).readOnly(),

    alwaysExpand: computed('expandedColumns.@each.canExpand', function() {
        return this.get('expandedColumns').any((column) => column.get('showExpanded') === true);
    }),

    expandedKeys: computed('expandedColumns.[]', function() {
        let keys = [];
        this.get('expandedColumns').forEach((column) => {
            // check if we have a specific expansion property
            let showExpanded = column.get('showExpanded');
            if (typeof showExpanded === 'string') {
                keys.pushObject(showExpanded);
            } else {
                // otherwise, try the value path
                if (column.get('valuePath')) {
                    keys.pushObject(column.get('valuePath'));
                }
            }
        });

        return keys;
    }),

    /**
     * Returns true if the table currently contains hidden columns
     *
     * @private
     * @property hasHiddenColumns
     * @type {Boolean}
     */
    hasExpandedColumns: computed('expandedColumns', function() {
        return !isEmpty(this.get('expandedColumns'));
    }),

    /**
     * Replace all the row's content with content of the argument. If argument is an empty array rows will be cleared.
     * @method setRows
     * @param  {Array} rows
     * @return {Array} rows
     */
    setRows(rows = []) {
        return this.get('rows').setObjects(createRows(rows, this));
    }
}) {
    constructor({columns, rows}) {
        super(columns, []);
        this.setRows(rows);
    }
}

/**
 * Creates an instance of the table class, mimicking the new Table constructor of the parent
 * @param  {Array} columns Table columns
 * @param  {Array} rows    Table rows
 * @return {Table}         Table class to use in ember-light-table
 */
export default function newTable(columns, rows) {
    return new Table({columns, rows});
}
