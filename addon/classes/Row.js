import {computed, defineProperty, observer} from '@ember/object';
import {alias} from '@ember/object/computed';
import LightRow from 'ember-light-table/classes/Row';
import {showExpanded} from '../utils/table';

/**
 * Function to create the canExpand computed property
 * @param  {array} keys Array of keys to watch
 * @return {computed}   Computed property
 */
function canExpand(keys) {
    return computed('table.{alwaysExpand,hasExpandedColumns}', ...keys, function() {
        // start with some short exits
        // no expanded columns is false
        if (!this.get('table.hasExpandedColumns')) {
            return false;
        }
        // if it has an expanded column that always shows, automatically true
        if (this.get('table.alwaysExpand')) {
            return true;
        }

        // try and find any column that has something
        return this.get('table.expandedColumns').any((column) => {
            return showExpanded(this.get('table'), this, column);
        });
    });
}

/**
 * Personal copy of Ember Light Table row to support the can expand property
 * @type {EmberObject}
 * @extends EmberLightTable/Row
 * @class Row
 */
export default class Row extends LightRow.extend({
    /**
     * Parent table instance
     * @property table
     * @type {Table}
     */

    /**
     * Checks if the row is currently allowed to expand
     * @type {boolean}
     */
    canExpand: alias('table.hasExpandedColumns'),

    /** Disables expanded state if canExpand is false */
    expanded: computed('canExpand', '_expanded', {
        get() {
            return this.get('canExpand') && this.get('_expanded');
        },
        set(_, value) {
            value &= this.get('canExpand');
            this.set('_expanded', value);
            return value;
        }
    }),

    /**
     * Observer to update canExpand if the expanded keys ever changes
     */
    // eslint-disable-next-line ember/no-observers
    updateColumns: observer('table.expandedKeys', function() {
        defineProperty(this, 'canExpand', canExpand(this.get('table.expandedKeys')));
    })
}) {
    constructor({content, table}) {
        super(content);
        this.set('table', table);
        defineProperty(this, 'canExpand', canExpand(table.get('expandedKeys')));
    }
}
